package com.maimieng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kingcos on 21/09/2016.
 */
public class Stack<T> {
    private List<T> m_pStack;
    private int m_pCapacity;

    private int m_iTop;

    public Stack(int m_pCapacity) {
        this.m_pCapacity = m_pCapacity;
        m_pStack = new ArrayList<T>();

        clearStack();
    }

    public void clearStack() {
        m_iTop = 0;
    }

    public boolean isEmpty() {
        return m_iTop == 0;
    }

    public boolean isFull() {
        return m_iTop >= m_pCapacity;
    }

    public boolean push(T element) {
        if (isFull()) {
            return false;
        }

        m_pStack.add(element);
        m_iTop += 1;
        return true;
    }

    public T pop() {
        if (isEmpty()) {
            return null;
        }
        m_iTop -= 1;
        T e = m_pStack.get(m_iTop);
        return e;
    }

    public void stackTraverse() {
        for (int i = m_iTop - 1; i >= 0; i --) {
            System.out.print(m_pStack.get(i) + " ");
        }

        System.out.println();
    }

}
