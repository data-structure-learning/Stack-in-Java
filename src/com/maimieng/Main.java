package com.maimieng;

public class Main {

    public static void main(String[] args) {
        System.out.println("testIntStack");
        testIntStack();

        System.out.println("testObjectStack");
        testObjectStack();

    }

    static void testIntStack() {
        Stack<Integer> s = new Stack<Integer>(5);
        s.push(2);
        s.push(5);

        s.stackTraverse();

        int e = s.pop();
        System.out.println(e);

        s.push(5);
        s.push(5);
        s.push(5);
        s.push(5);

        s.stackTraverse();

        s.pop();
        s.pop();

    }

    static void testObjectStack() {
        Stack<Coordinate> s = new Stack<Coordinate>(5);
        s.push(new Coordinate(2, 2));
        s.push(new Coordinate(5, 5));

        s.stackTraverse();

        Coordinate e = s.pop();
        System.out.println(e);

        s.push(new Coordinate(7, 7));
        s.push(new Coordinate(12, 12));
        s.push(new Coordinate(19, 19));
        s.push(new Coordinate(31, 31));
        s.push(new Coordinate(50, 50));

        s.stackTraverse();

        s.pop();
        s.pop();

    }
}
