package com.maimieng;

/**
 * Created by kingcos on 21/09/2016.
 */
public class Coordinate {
    private int x;
    private int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        String s = "(" + x + ", " + y + ")";
        return s;
    }
}
